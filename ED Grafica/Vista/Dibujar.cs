﻿using ED_Grafica.Modelo;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ED_Grafica.Vista
{
    class Dibujar
    {
        Graphics gr;
        Pen pincel;
        public Dibujar(Graphics gr)
        {
            this.gr = gr;
            pincel = new Pen(Color.Black, 3);
            
            
        }
        private void draw(Poligono poligon)
        {
            Punto pi = poligon.getPunto(0);
            for (int i = 0; i < poligon.getLength() - 1; i++)
            {
                int px1 = (int)Math.Ceiling(poligon.getPunto(i).getX());
                int py1 = (int)Math.Ceiling(poligon.getPunto(i).getY());
                Point pa = new Point(px1, py1);

                int px2 = (int)Math.Ceiling(poligon.getPunto(i + 1).getX());
                int py2 = (int)Math.Ceiling(poligon.getPunto(i + 1).getY());
                Point pb = new Point(px2, py2);

                gr.DrawLine(pincel, pa, pb);

                if (((int)poligon.getType() == 2) && (i == poligon.getLength() - 2))//poligono cerrado
                {
                    pa.X = (int)Math.Ceiling(pi.getX());
                    pa.Y = (int)Math.Ceiling(pi.getY());
                    gr.DrawLine(pincel, pa, pb);
                }
            }
        }

        private void draw(Objeto obj)
        {
            for (int i = 0; i < obj.getLength(); i++)
            {
                Poligono poligono1 = obj.getPoligon(i);
                draw(poligono1);
            }
        }

        public void draw(Escenario esc)
        {
            for (int i = 0; i < esc.getLength(); i++)
            {
                Objeto obj = esc.getObject(i);
                draw(obj);
            }

        }    
    }
}
