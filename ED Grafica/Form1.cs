﻿using ED_Grafica.Modelo;
using ED_Grafica.Vista;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ED_Grafica
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void dibujarCasaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //techo, poligono cerrado
            Punto p1 = new Punto(120, 130);
            Punto p2 = new Punto(130, 150);
            Punto p3 = new Punto(180, 150);
            Punto p4 = new Punto(170, 130);

            Poligono poligono = new Poligono();
            poligono.addPunto(p1);
            poligono.addPunto(p2);
            poligono.addPunto(p3);
            poligono.addPunto(p4);

            Punto p6 = new Punto(120, 130);
            Punto p7 = new Punto(110, 150);
            Punto p8 = new Punto(130, 150);

            Poligono poligono1 = new Poligono();
            poligono1.addPunto(p6);
            poligono1.addPunto(p7);
            poligono1.addPunto(p8);


            Punto p9 = new Punto(110,150);
            Punto p10 = new Punto(130,150);
            Punto p11 = new Punto(130,170);
            Punto p12 = new Punto(110,170);
            Poligono poligono2 = new Poligono();
            poligono2.addPunto(p9);
            poligono2.addPunto(p10);
            poligono2.addPunto(p11);
            poligono2.addPunto(p12);

            //
            Punto p13 = new Punto(130, 150);
            Punto p14 = new Punto(180, 150);
            Punto p15 = new Punto(180, 170);
            Punto p16 = new Punto(130, 170);
            Poligono poligono3 = new Poligono();
            poligono3.addPunto(p13);
            poligono3.addPunto(p14);
            poligono3.addPunto(p15);
            poligono3.addPunto(p16);


            Objeto objeto = new Objeto();
            objeto.addPoligon(poligono);
            objeto.addPoligon(poligono1);
            objeto.addPoligon(poligono2);
            objeto.addPoligon(poligono3);
     
           
            Escenario escenario = new Escenario();
            escenario.addObject(objeto);

            System.Windows.Forms.DataVisualization.Charting.ChartArea c = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.ElementPosition p;
            p = new System.Windows.Forms.DataVisualization.Charting.ElementPosition(50, 50, 200, 200);
            c.Position = p;
            
            Dibujar dibujar = new Dibujar(this.CreateGraphics());
            dibujar.draw(escenario);
            
        }
    }
}
