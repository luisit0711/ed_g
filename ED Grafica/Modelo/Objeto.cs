﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ED_Grafica.Modelo
{
    class Objeto
    {
        LinkedList<Poligono> listObject;

        public Objeto()
        {
            listObject = new LinkedList<Poligono>();
        }

        public void addPoligon(Poligono poligon)
        {
            listObject.AddLast(poligon);
        }

        public int getLength()
        {
            return listObject.Count();
        }

        public void deletePoligon(Poligono poligon)
        {
            listObject.Remove(poligon);
        }

        public Poligono getPoligon(int index)
        {
            return listObject.ElementAt(index);
        }
    }
}
