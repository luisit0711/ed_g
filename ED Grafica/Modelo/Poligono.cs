﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ED_Grafica.Modelo
{
    public class Poligono
    {
        LinkedList<Punto> listPoligon;
        public enum type {open=1,close=2};
        type t;
        public Poligono()
        {
            listPoligon = new LinkedList<Punto>();
            t = type.close;
        }

        public Poligono(type t)
        {
            listPoligon = new LinkedList<Punto>();
            this.t = t;
        }
        public type getType()
        {
            return this.t;
        }
        
        public void addPunto(Punto point)
        {
            listPoligon.AddLast(point);
        }  

        public int getLength()
        {
            return listPoligon.Count();
        }

        public void deletePoint(Punto point)
        {
            listPoligon.Remove(point);
        }

        public Punto getPunto(int index)
        {
            return listPoligon.ElementAt(index);
        }
    }
}
