﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ED_Grafica.Modelo
{
    class Escenario
    {
        LinkedList<Objeto> listEsc;

        public Escenario()
        {
            listEsc = new LinkedList<Objeto>();
        }

        public void addObject(Objeto obj)
        {
            listEsc.AddLast(obj);
        }

        public int getLength()
        {
            return listEsc.Count();
        }

        public void deleteObject(Objeto obj)
        {
            listEsc.Remove(obj);
        }

        public Objeto getObject(int index)
        {
            return listEsc.ElementAt(index);
        }
    }
}
