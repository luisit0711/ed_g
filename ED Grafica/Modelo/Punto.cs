﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ED_Grafica.Modelo
{
    public class Punto
    {
        float x, y,z;
        public Punto()
        {
            x = y = 0;
            z = 1;
        }
        public Punto(float x, float y)
        {
            this.x = x; this.y = y; z = 1;
        }

        public Punto(float x, float y, float z)
        {
            this.x = x;this.y = y;this.z = z;
        }

        public void setX(float x)
        {
            this.x = x;
        }

        public void setY(float y)
        {
            this.y = y;
        }

        public void setZ(float z)
        {
            this.z = z;
        }
        public float getX()
        {
            return this.x;
        }

        public float getY()
        {
            return this.y;
        }

        public float getZ()
        {
            return z;
        }
    }
}
